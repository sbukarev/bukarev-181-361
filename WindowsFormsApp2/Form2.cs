﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            button1.Font = new Font("Microsoft Sans Serif", 10);
            button2.Font = new Font("Microsoft Sans Serif", 10);
            label1.Font = new Font("Microsoft Sans Serif", 10);
            radioButton1.Font = new Font("Microsoft Sans Serif", 10);
            radioButton2.Font = new Font("Microsoft Sans Serif", 10);
            radioButton3.Font = new Font("Microsoft Sans Serif", 10);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                Data.Value = 1;
            }
            if (radioButton2.Checked == true)
            {
                Data.Value = 2;
            }
            if (radioButton3.Checked == true)
            {
                Data.Value=3;
            }
            Form ifrm = new Form1();
            ifrm.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
