﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Button[] buttons = new Button[9];

        public Form1()
        {
            InitializeComponent();
            chlvl();
            button1.Font = new Font("Microsoft Sans Serif", 15);
            button2.Font = new Font("Microsoft Sans Serif", 15);
            button3.Font = new Font("Microsoft Sans Serif", 15);
            button4.Font = new Font("Microsoft Sans Serif", 15);
            button5.Font = new Font("Microsoft Sans Serif", 15);
            button6.Font = new Font("Microsoft Sans Serif", 15);
            button7.Font = new Font("Microsoft Sans Serif", 15);
            button8.Font = new Font("Microsoft Sans Serif", 15);
            button9.Font = new Font("Microsoft Sans Serif", 15);
            button10.Font = new Font("Microsoft Sans Serif", 10);
            button11.Font = new Font("Microsoft Sans Serif", 10);
            button1.Text = passw[0];
            button2.Text = passw[1];
            button3.Text = passw[2];
            button4.Text = passw[3];
            button5.Text = passw[4];
            button6.Text = passw[5];
            button7.Text = passw[6];
            button8.Text = passw[7];
            button9.Text = passw[8];
            textBox1.Font = new Font("Microsoft Sans Serif", 12);
            textBox1.Text = "Для взлома терминала подберите верный пароль";


        }
        public int tryes;
        public string[] passw = new string[9];
        public int rightpoz = 0;
        public string werniiparol;
        public int lvl;
        public void chlvl()
        {
            if (Data.Value == 1)
            {
                lvl = 4;
                tryes = 4;
            }
            if (Data.Value == 2)
            {
                lvl = 4;
                tryes = 3;
            }
            if (Data.Value == 3)
            {
                lvl = 5;
                tryes = 3;
            }
            passwgenerator();
        }
        public void passwgenerator()
        {
            var arr1 = new List<string>();
            int i = 0;
            int j = 0;
            string vernii_parol;
            Random rnd = new Random();
            string c;
            string[] arr = new string[36] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "G", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
         "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
            for (i = 0; i <= 5; i++)
            {
                while (true){
                    int value = rnd.Next(0, 35);
                    c = arr[value];
                    bool contains = arr1.Contains(c);
                    if (contains==false){
                        arr1.Add(c);
                        break;
            }
                }

            }
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < lvl; j++)
                {
                    int value = rnd.Next(0, 4);
                    c = arr1[value];
                    passw[i] += c;
                }
            }
            int value2 = rnd.Next(0, 8);
            werniiparol = passw[value2];
        }
    public void ttry()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[0] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[0][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[0];
            }
        }
    }
    public void ttry1()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[1] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[1][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[1];
            }
        }
    }
    public void ttry2()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[2] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[2][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[2];
            }
        }
    }
    public void ttry3()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[3] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[3][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[3];
            }
        }
    }
    public void ttry4()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[4] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[4][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[4];
            }
        }
    }
    public void ttry5()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[5] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[5][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[5];
            }
        }
    }
    public void ttry6()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[6] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[6][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[6];
            }
        }
    }
    public void ttry7()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[7] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[7][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[7];
            }
        }
    }
    public void ttry8()
    {
        rightpoz = 0;
        int i = 0;
        if (passw[8] == werniiparol)
        {
            textBox1.Text = "Терминал успешно взломан";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = true;
        }
        else
        {
            for (i = 0; i < lvl; i++)
            {
                if (passw[8][i] == werniiparol[i])
                {
                    rightpoz += 1;
                }
            }
            tryes -= 1;
            if (tryes == 0)
            {
                textBox1.Text = "Терминал заблокирован, для доступа обратитесь к администратору";
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = true;
                button11.Visible = true;
            }
            else
            {
                textBox1.Text = "Верных символов на верных позициях: " + rightpoz + ", попыток осталось: " + tryes + ", введённый вами пароль: " + passw[8];
            }
        }

    }
    private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            ttry();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ttry1();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            ttry2();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            ttry3();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            ttry4();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            ttry5();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            ttry6();
        }
        private void button8_Click(object sender, EventArgs e)
        {
            ttry7();
        }
        private void button9_Click(object sender, EventArgs e)
        {
            ttry8();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
      
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            Form ifrm = new Form2();
            ifrm.Show();
            this.Hide();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
